# coding=utf-8

from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray


class TestArray(TestCase):
	def test___init__(self):

		# simple init tests, other things are tester later
		try:
			a = BaseArray((2,))
			a = BaseArray([2, 5])
			a = BaseArray((2, 5), dtype=int)
			a = BaseArray((2, 5), dtype=float)
		except Exception as e:
			self.fail(f'basic constructor test failed, with exception {e:}')

		# test invalid parameters as shape
		with self.assertRaises(Exception) as cm:
			a = BaseArray(5)
		self.assertEqual(('shape 5 is not a tuple or list',),
						 cm.exception.args)

		# test invalid value in shape
		with self.assertRaises(Exception) as cm:
			a = BaseArray((4, 'a'))
		self.assertEqual(('shape (4, \'a\') contains a non integer a',),
						 cm.exception.args)

		# TODO test invalid dtype

		# test valid data param, list, tuple
		a = BaseArray((3, 4), dtype=int, data=list(range(12)))
		a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

		# test invalid data param
		# invalid data length
		with self.assertRaises(Exception) as cm:
			a = BaseArray((2, 4), dtype=int, data=list(range(12)))
		self.assertEqual(('shape (2, 4) does not match provided data length 12',),
						 cm.exception.args)

		# inconsistent data type
		with self.assertRaises(Exception) as cm:
			a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
		self.assertEqual(('provided data does not have a consistent type',),
						 cm.exception.args)

	def test_shape(self):
		a = BaseArray((5,))
		self.assertTupleEqual(a.shape, (5,))
		a = BaseArray((2, 5))
		self.assertTupleEqual(a.shape, (2, 5))
		a = BaseArray((3, 2, 5))
		self.assertEqual(a.shape, (3, 2, 5))

	def test_search(self):
		# test SEARCH
		a= BaseArray((3,3), dtype=int, data=(0,0,0,2,0,0,0,2,2))	
		self.assertEqual(a.search(2), ((1,0),(2,1),(2,2)))

	def test_sorting(self):
		# test ROW SORTING
		a= BaseArray((3,3), dtype=int, data=(0,1,1,4,0,-2,1,3,0))
		a.sort_rows()
		self.assertEqual(a.data, [0,1,1,-2,0,4,0,1,3])
		# test COLUMN SORTING
		a= BaseArray((3,3), dtype=int, data=(0,1,1,4,0,-2,1,3,0))
		a.sort_columns()
		self.assertEqual(a.data, [0,0,-2,1,1,0,4,3,1])
		# test 1D ARRAYS
		a=BaseArray((1,9), dtype=int, data=(0,1,1,-2,0,4,0,1,3))
		a.sort_rows()
		self.assertEqual(a.data, [-2,0,0,0,1,1,1,3,4])

	def test_mat_ops(self):
		# ADDITION (scalar/matrix)
		A=BaseArray((2,3), dtype=int, data=(1,3,1,1,0,0))
		B=BaseArray((2,3), dtype=int, data=(0,0,5,7,5,0))
		A.add(B)
		self.assertEqual(A.data, [1,3,6,8,5,0])
		A=BaseArray((2,3), dtype=int, data=(1,3,6,8,5,0))
		A.add(3)
		self.assertEqual(A.data, [4,6,9,11,8,3])		
		# SUBSTRACTION (scalar/matrix)
		A=BaseArray((2,2,2), dtype=int, data=(1,2,3,4,5,6,7,8))
		B=BaseArray((2,2,2), dtype=int, data=(1,1,1,1,1,1,1,1,))
		A.subtract(B)
		self.assertEqual(A.data, [0,1,2,3,4,5,6,7])
		A=BaseArray((2,3), dtype=int, data=(4,6,9,11,8,3))
		A.subtract(10)
		self.assertEqual(A.data, [-6,-4,-1,1,-2,-7])
		# MULTIPLICATION (scalar/matrix)
		A=BaseArray((2,2), dtype=int, data=(1,2,3,4))
		B=BaseArray((2,2), dtype=int, data=(2,0,1,2))
		C=A.multiply(B)
		self.assertEqual(C.data, [4,4,10,8])
		A=BaseArray((2,3), dtype=int, data=(4,-6,9,11,-8,3))
		A.multiply(-2)
		self.assertEqual(A.data, [-8,12,-18,-22,16,-6])
		# DIVISION (scalar)
		A=BaseArray((2,2), dtype=float, data=(12,3.7,6,7))
		A.divide(-3.7)
		self.assertEqual(A.data, [-3.24,-1.0,-1.62,-1.89])
		# LOGARITHM (matrix)
		A=BaseArray((2,2), dtype=float, data=(2,4,1.5,2))
		A.logarithm(10)
		self.assertEqual(A.data, [0.3,0.6,0.18,0.3])
		# POWE (matrix)
		A=BaseArray((2,2), dtype=float, data=(2,4,1.5,2))
		A.pow(3.14)
		self.assertEqual(A.data, [8.82,77.71,3.57,8.82])
		# SECONDARY MULTIPLICATION
		A=BaseArray((2,2), dtype=int, data=(1,2,3,4))
		B=BaseArray((2,2), dtype=int, data=(2,0,1,2))
		A.multiply_secondary(B)
		self.assertEqual(A.data, [2,0,3,8])


	def test_dtype(self):
		# test if setting a type returns the set type
		a = BaseArray((1,))
		self.assertIsInstance(a[0], float)
		a = BaseArray((1,), int)
		self.assertIsInstance(a[0], int)
		a = BaseArray((1,), float)
		self.assertIsInstance(a[0], float)

	def test_get_set_item(self):
		# test if setting a value returns the value
		a = BaseArray((1,))
		a[0] = 1
		self.assertEqual(1, a[0])
		# test for multiple dimensions
		a = BaseArray((2, 1))
		a[0, 0] = 1
		a[1, 0] = 1
		self.assertEqual(1, a[0, 0])
		self.assertEqual(1, a[1, 0])

		# test with invalid indices
		a = BaseArray((2, 2))
		with self.assertRaises(Exception) as cm:
			a[3, 0] = 0
		self.assertEqual(('indice (3, 0), axis 0 out of bounds (0, 2)',),
						 cm.exception.args)

		# test invalid type of ind
		with self.assertRaises(Exception) as cm:
			a[1, 1.1] = 0
		self.assertEqual(cm.exception.args,
						 ('(1, 1.1) is not a valid indice',))

		with self.assertRaises(Exception) as cm:
			a[1, 'a'] = 0
		self.assertEqual(('(1, \'a\') is not a valid indice',),
						 cm.exception.args)

		# test invalid number of ind
		with self.assertRaises(Exception) as cm:
			a[1] = 0
		self.assertEqual(('indice (1,) is not valid for this myarray',),
						 cm.exception.args)

		# test data intialized via data parameter
		a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
		self.assertEqual(3, a[0, 0, 3])
		self.assertEqual(13, a[1, 0, 1])

		# TODO enter invalid type


	def test_iter(self):
		a = BaseArray((2, 3), dtype=int)
		a[0, 0] = 1
		a[0, 1] = 2
		a[0, 2] = 3
		a[1, 0] = 4
		a[1, 1] = 5
		a[1, 2] = 6

		a_vals = [v for v in a]
		self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

	def test_reversed(self):
		a = BaseArray((2, 3), dtype=int)
		a[0, 0] = 1
		a[0, 1] = 2
		a[0, 2] = 3
		a[1, 0] = 4
		a[1, 1] = 5
		a[1, 2] = 6

		a_vals = [v for v in reversed(a)]
		self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

	def test_contains(self):
		a = BaseArray((1,), dtype=float)
		a[0] = 1

		self.assertIn(1., a)
		self.assertIn(1, a)
		self.assertNotIn(0, a)

	def test_shape_to_steps(self):
		shape = (4, 2, 3)
		steps_exp = (6, 3, 1)
		steps_res = ndarray._shape_to_steps(shape)
		self.assertTupleEqual(steps_exp, steps_res)

		shape = [1]
		steps_exp = (1,)
		steps_res = ndarray._shape_to_steps(shape)
		self.assertTupleEqual(steps_exp, steps_res)

		shape = []
		steps_exp = ()
		steps_res = ndarray._shape_to_steps(shape)
		self.assertTupleEqual(steps_exp, steps_res)

	def test_multi_to_lin_ind(self):
		# shape = 4, 2,
		# 0 1
		# 2 3
		# 4 5
		# 6 7
		steps = 2, 1
		multi_lin_inds_pairs = (((0, 0), 0),
								((1, 1), 3),
								((3, 0), 6),
								((3, 1), 7))
		for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
			lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
			self.assertEqual(lin_ind_exp, lin_ind_res)

		# shape = 2, 3, 4
		#
		# 0 1 2 3
		# 4 5 6 7
		#
		#  8  9 10 11
		# 12 13 14 15
		steps = 8, 4, 1
		multi_lin_inds_pairs = (((0, 0, 0), 0),
								((1, 1, 0), 12),
								((0, 0, 3), 3),
								((1, 1, 2), 14))

		for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
			lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
			self.assertEqual(lin_ind_res, lin_ind_exp)

	def test_is_valid_instance(self):
		self.assertTrue(ndarray._is_valid_indice(1))
		self.assertTrue(ndarray._is_valid_indice((1,0)))

		self.assertFalse(ndarray._is_valid_indice(()))
		self.assertFalse(ndarray._is_valid_indice([1]))
		self.assertFalse(ndarray._is_valid_indice(1.0))
		self.assertFalse(ndarray._is_valid_indice((1, 2.)))

	def test_multi_ind_iterator(self):
		shape = (30,)
		ind = (slice(10, 20, 3),)
		ind_list_exp = ((10,),
						(13,),
						(16,),
						(19,))
		ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
		for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
			self.assertTupleEqual(ind_exp, ind_res)

		shape = (5, 5, 5)
		ind = (2, 2, 2)
		ind_list_exp = ((2, 2, 2),)
		ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
		for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
			self.assertTupleEqual(ind_exp, ind_res)

		ind = (2, slice(3), 2)
		ind_list_exp = ((2, 0, 2),
						(2, 1, 2),
						(2, 2, 2),
						)
		ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
		for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
			self.assertTupleEqual(ind_exp, ind_res)

		ind = (slice(None, None, 2), slice(3), slice(2, 4))
		ind_list_exp = ((0, 0, 2), (0, 0, 3),
						(0, 1, 2), (0, 1, 3),
						(0, 2, 2), (0, 2, 3),
						(2, 0, 2), (2, 0, 3),
						(2, 1, 2), (2, 1, 3),
						(2, 2, 2), (2, 2, 3),
						(4, 0, 2), (4, 0, 3),
						(4, 1, 2), (4, 1, 3),
						(4, 2, 2), (4, 2, 3),
						)
		ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
		for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
			self.assertTupleEqual(ind_exp, ind_res)



