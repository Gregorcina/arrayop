# coding=utf-8

from typing import Tuple
from typing import List
from typing import Union
from math import log, e, pi, pow

from myarray.merge_sort import mergeSort

class BaseArray:
	"""
	Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
	številskega tipa: int ali float.

	Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
	podatke za inicializacijo (privzeto vse 0).

	Primeri ustvarjanja:
	a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
	a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
	a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
																			   #tipa float, vrednosti 1. in 2.
	a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


	Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
	Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
	 Primeri dostopanja :

	a = BaseArray((2, 3, 4, 5))
	a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
	v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

	a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

	Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

	a.shape # to vrne vrednost  (2, 3, 4, 5)
	a.dtype # vrne tip int

	Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

	a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
	for v in a: # for zanka izpiše vrednost od 1 do 8
		print(v)
	for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
		print(v)

	2 in a # vrne True
	-1 in a # vrne False
	"""
	def __init__(self,
		shape: Tuple[int],
		dtype: type = None,
		data: Union[Tuple, List] = None):
		"""
		Create an initialize a multidimensional myarray of a selected data type.
		Init the myarray to 0.

		:param shape: tuple or list of integers, shape of constructed myarray
		:param dtype: type of data stored in myarray, float or int
		:param data: list of tuple of data values, must contain consistent types and
					 have a length that matches the shape.
		"""

		if not isinstance(shape, (tuple, list)):
			raise Exception(f'shape {shape:} is not a tuple or list')
		for v in shape:
			if not isinstance(v, int):
				raise Exception(f'shape {shape:} contains a non integer {v:}')

		self.__shape = (*shape, )

		self.__steps = _shape_to_steps(shape)

		if dtype is None:
			dtype = float
		if dtype not in [int, float]:
			raise Exception('Type must by int or float.')
		self.__dtype = dtype

		n = 1
		for s in self.__shape:
			n *= s

		if data is None:
			if dtype == int:
				self.__data = [0] * n
			elif dtype == float:
				self.__data = [0.0] * n
		else:
			if len(data) != n:
				raise Exception(
				 f'shape {shape:} does not match provided data length {len(data):}'
				)

			for d in data:
				if not isinstance(d, (int, float)):
					raise Exception(
					 f'provided data does not have a consistent type')
			self.__data = [d for d in data]


	@property
	def data(self):
		"""
		:return: a list containing stored data.
		"""
		return self.__data

	@property
	def shape(self):
		"""
		:return: a tuple representing the shape of the myarray.
		"""
		return self.__shape

	@property
	def dtype(self):
		"""
		:return: the type stored in the myarray.
		"""
		return self.__dtype


	def print(self):
		for i, item in enumerate(self.__data):
			print('{:>5}'.format(item), end=" ")  # Skrbi za formatiran izpis
			if(i+1)%self.shape[1]==0:
				if((i+1)%(self.shape[0]*self.shape[1])==0):
					print("\n\n", end="")
				else:
					print("\n", end="")


	def sort_rows(self):
		if len(self.shape) > 2:
			raise Exception(f'Given table is more than two dimensional! (Sorting works only on 1D/2D tables)')
		
		y = 0
		vrsticaData = []			
		while y < self.shape[0]:
			x = 0
			while x < self.shape[1]:
				vrsticaData.append(self[y,x])
				x += 1
			
			x=0
			vrsticaData=mergeSort(vrsticaData)
			while x < self.shape[1]:
				self[y,x]=vrsticaData[x]
				x+=1
	
			vrsticaData.clear()				
			y+=1


	def search(self, value):
		searchResult=()
		y=0
		while y < self.shape[0]:
			x=0
			while x < self.shape[1]:
				if self[y,x] == value:
					searchResult+=((y,x),)

				x+=1

			y+=1

		return searchResult


	def sort_columns(self):
		if len(self.shape) > 2:
			raise Exception(f'Given table is more than two dimensional! (Sorting works only on 1D/2D tables)')
		elif (self.shape[0] == 1) or (self.shape[1] == 1):
			return

		y = 0
		vrsticaData = []			
		while y < self.shape[0]:
			x = 0
			while x < self.shape[1]:
				vrsticaData.append(self[x,y])
				x += 1
			
			x=0
			vrsticaData=mergeSort(vrsticaData)
			while x < self.shape[1]:
				self[x,y]=vrsticaData[x]
				x+=1
	
			vrsticaData.clear()				
			y+=1


	def subtract(self, subtrahend):
		if (type(subtrahend) is float) or (type(subtrahend) is int):
			for i, item in enumerate(self.__data):
				self.__data[i]=item-subtrahend		
		elif (type(subtrahend) is BaseArray):
			if self.shape != subtrahend.shape:
				raise Exception(f'Arrays do not match!\n (Subtraction only works between two arrays with equal sizes)')
			for i, item in enumerate(self.__data):
				self.__data[i]-=subtrahend.data[i]
		else:
			raise Exception(f'Wrong parameter type!\n (Subtraction only works between Array-Number or Array-Array)')


	def add(self, addend):
		if (type(addend) is float) or (type(addend) is int):
			for i, item in enumerate(self.__data):
				self.__data[i]=item+addend		
		elif (type(addend) is BaseArray):
			if self.shape != addend.shape:
				raise Exception(f'Arrays do not match!\n (Addition only works between two arrays with equal sizes)')
			for i, item in enumerate(self.__data):
				self.__data[i]+=addend.data[i]
		else:
			raise Exception(f'Wrong parameter type!\n (Addition only works between Array+Number or Array+Array)')


	def multiply(self, multiplier):
		if (type(multiplier) is float) or (type(multiplier) is int):
			for i, item in enumerate(self.__data):
				self.__data[i]=round(item*multiplier,2)		
		else:
			if (self.__shape[0] != multiplier.__shape[1]):
				raise Exception(f'Cannot multiply!\n(Dimensions of tables must match according to linear algebra\'s law)')

			res=BaseArray((self.__shape[0],multiplier.__shape[1]))

			for i in range(self.__shape[0]): 			# st. vrstic mat1
				for j in range(multiplier.__shape[1]): 			#st stolpcev mat2
					for k in range(multiplier.__shape[0]): 		# st vrstic mat2	
						res[i,j] += round(self[i,k] * multiplier[k,j],2)

			return res

	def multiply_secondary(self, multiplier):
		if (type(multiplier) is BaseArray):
			if self.shape != multiplier.shape:
				raise Exception(f'Arrays do not match!\n (Addition only works between two arrays with equal sizes)')
			for i, item in enumerate(self.__data):
				self.__data[i]*=multiplier.data[i]
		else:
			raise Exception(f'Wrong parameter type!\n (Multiplication2 only works between two Arrays of the same size)')

	def divide(self, divisor):
		if (type(divisor) is float) or (type(divisor) is int):
			for i, item in enumerate(self.__data):
				self.__data[i]=round(item/divisor, 2)
		else:
			raise Exception(f'Wrong parameter type!\n (Division only works between Array and a scalar)')


	def logarithm(self, base):
		if ((type(base) is float) or (type(base) is int)):
			for i, item in enumerate(self.__data):
				self.__data[i]=round(log(item, base),2)
		else:
			raise Exception(f'Wrong parameter type!\n ()')


	def pow(self, eksponent):
		if ((type(eksponent) is float) or (type(eksponent) is int)):
			for i, item in enumerate(self.__data):
				self.__data[i]=round(pow(item, eksponent),2)	
		else:
			raise Exception(f'Wrong parameter type!\n ()')

	def __test_indice_in_range(self, ind):
		"""
		Test if the given indice is in within the range of this myarray.
		:param ind: the indices used
		:return: True if indices are valid, False otherwise.
		"""
		for ax in range(len(self.__shape)):
			if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
				raise Exception(
				 'indice {:}, axis {:d} out of bounds (0, {:})'.format(
				  ind, ax, self.__shape[ax]))


	def __getitem__(self, indice):
		"""
		Return a value from this myarray.
		:param indice: indice to this myarray, integer or tuple
		:return: value at indice
		"""
		if not type(indice) is tuple:
			indice = (indice, )

		if not _is_valid_indice(indice):
			raise Exception(f'{indice} is not a valid indice')

		if len(indice) != len(self.__shape):
			raise Exception(f'indice {indice} is not valid for this myarray')

		self.__test_indice_in_range(indice)
		key_lin = _multi_to_lin_ind(indice, self.__steps)
		return self.__data[key_lin]


	def __setitem__(self, indice, value):
		"""
		Set a value in this myarray.
		:param indice: valued indice to a position, integer of tuple
		:param value: value to set
		:return: does not return
		"""
		if not type(indice) is tuple:
			indice = (indice, )

		if not _is_valid_indice(indice):
			raise Exception(f'{indice} is not a valid indice')

		if len(indice) != len(self.__shape):
			raise Exception(f'indice {indice} is not valid for this myarray')

		self.__test_indice_in_range(indice)
		key_lin = _multi_to_lin_ind(indice, self.__steps)
		self.__data[key_lin] = value

	def __iter__(self):
		for v in self.__data:
			yield v

	def __reversed__(self):
		for v in reversed(self.__data):
			yield v

	def __contains__(self, item):
		return item in self.__data

def _multi_ind_iterator(multi_inds, shape):
	inds = multi_inds[0]
	s = shape[0]

	if type(inds) is slice:
		inds_itt = range(s)[inds]
	elif type(inds) is int:
		inds_itt = (inds, )

	for ind in inds_itt:
		if len(shape) > 1:
			for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
				yield (ind, ) + ind_tail
		else:
			yield (ind, )


def _shape_to_steps(shape):
	dim_steps_rev = []
	s = 1
	for d in reversed(shape):
		dim_steps_rev.append(s)
		s *= d
	steps = tuple(reversed(dim_steps_rev))
	return steps


def _multi_to_lin_ind(inds, steps):
	i = 0
	for n, s in enumerate(steps):
		i += inds[n] * s
	return i


def _is_valid_indice(indice):
	if isinstance(indice, tuple):
		if len(indice) == 0:
			return False
		for i in indice:
			if not isinstance(i, int):
				return False
	elif not isinstance(indice, int):
		return False
	return True
