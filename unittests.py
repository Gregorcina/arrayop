from myarray.basearray import BaseArray
from tests.myarray.test_array import TestArray

import sys
# A=BaseArray((2, 5), dtype=float)

def test_search():
	test=TestArray()
	test.test_search()
	print('test_search OK')

def test_sorting():
	test=TestArray()
	test.test_sorting()
	print('test_sorting OK')

def test_mat_ops():
	test=TestArray()
	test.test_mat_ops()
	print('test_mat_ops OK')


# test.test_dtype()
# test.test_get_set_item()
# test.test_iter()
# test.test_reversed()
# test.test_contains()

if __name__ == '__main__':
    globals()[sys.argv[1]]()
